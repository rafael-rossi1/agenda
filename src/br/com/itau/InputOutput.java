package br.com.itau;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class InputOutput {

    public static void imprimirMensagemIncial(){
        System.out.println("Bem vindo ao sistema");
        System.out.println("Informe a opção desejada e tecle <ENTER>");
        System.out.println("    ( 1 ) - Agenda");
        System.out.println("    ( 2 ) - Fim");
    }

    public static Map<String , String > solicitarEntrada(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Opção :");
        String opcao = scanner.nextLine();

        Map<String , String > dadosEntrada = new HashMap<>();
        dadosEntrada.put("opcao", opcao);

        return dadosEntrada;
    }
    public static Map<String , String > solicitarDados(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Digite a quantidade de Membros :");
        Integer quantidade = scanner.nextInt();

        Map<String , String > dados = new HashMap<>();
        dados.put("quantidade", quantidade.toString());

        return dados;
    }

    public static Map<String , String > solicitarDadosAgenda(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Digite o email:");
        String email = scanner.nextLine();

        Map<String , String > dadosAgenda = new HashMap<>();
        dadosAgenda.put("email", email);

        return dadosAgenda;
    }
    public static void imprimirMensagemMenu2(){
        System.out.println("Bem vindo ao sistema");
        System.out.println("Informe a opção desejada e tecle <ENTER>");
        System.out.println("    ( 3 ) - Consulta");
        System.out.println("    ( 4 ) - Exclui");
    }

    public static Map<String , String > solicitarEntrada2(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Opção :");
        String opcao2 = scanner.nextLine();

        Map<String , String > dadosEntrada2 = new HashMap<>();
        dadosEntrada2.put("opcao", opcao2);

        return dadosEntrada2;
    }
    public static void imprimirMensagemMenu3(){
        System.out.println("Informe o email para excluir");
    }
    public static Map<String , String > solicitarEntrada3() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Email :");
        String email = scanner.nextLine();

        Map<String, String> dadosEntrada3 = new HashMap<>();
        dadosEntrada3.put("email", email);

        return dadosEntrada3;
    }
}

