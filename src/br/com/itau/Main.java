package br.com.itau;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static java.lang.System.out;

public class Main {

    public static void main(String[] args) {
        InputOutput.imprimirMensagemIncial();
        Map<String, String> dadosEntrada = InputOutput.solicitarEntrada();
        String opcao = dadosEntrada.get("opcao");

        if ("1".equals(opcao)) {
            ArrayList<String> agenda = new ArrayList();
            Map<String, String> dados = InputOutput.solicitarDados();

            int quantidade = Integer.parseInt(dados.get("quantidade"));
            for (int i = 0; i < quantidade; i++) {
                Map<String, String> dadosAgenda = InputOutput.solicitarDadosAgenda();
                String email = dadosAgenda.get("email");
                agenda.add(i, email);
                out.println(agenda.get(i));

            }
            int i = 0;
            out.println("Inicio da agenda de email's");
            for (String contato : agenda) {
                out.println(contato);
                i++;
            }
            out.println("Final da agenda de email's");
            InputOutput.imprimirMensagemMenu2();
            Map<String, String> dadosEntrada2 = InputOutput.solicitarEntrada2();
            String opcao2 = dadosEntrada2.get("opcao");
            if ("3".equals(opcao2)) {
                i = 0;
                out.println("Inicio da agenda de email's");
                for (String contato : agenda) {
                    out.println(contato);
                    i++;
                }
            } else if ("4".equals(opcao2)) {
                InputOutput.imprimirMensagemMenu3();
                Map<String, String> dadosEntrada3 = InputOutput.solicitarEntrada3();
                String email = dadosEntrada3.get("email");
                for (String contato3 : agenda) {
                    agenda.remove(email);
                    out.println("Inicio da agenda de email's");
                    for (String contato2 : agenda) {
                        out.println(contato2);
                        i++;
                    }

                }
            } else {
                out.println("Agenda finalizada");
                System.exit(0);
            }


        }
    }
}